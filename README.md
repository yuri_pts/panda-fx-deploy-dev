# AOS animation JS library readme: https://github.com/michalsnik/aos/blob/next/README.md

## Style guide
- Indent - 4 spaces
- All yaml strings must be encapsulated in "double quotes"


## Custom features:

- Menu item custom class, done by adding:
'custom_class: your_class' in the page front matter.

- Menu include in main, done by adding:
'menu: main' in page front matter.

- Menu include in footer done by adding:
'secondary-menu: footer' in page front matter
'footer-weight: [number]' for sorting

- Exclude from ALL menus (main, mobile, inner page, footer):
'exclude: true' in page front matter

- Exclude from sitemap.xml - every page that you mark with:

```
---
something
---

<!-- Page content --->

```

will be included in sitemap. So it is important to exclude those elements (.js and .php files, hidden pages etc.)
from sitemap. this is done with
'exclude_from_sitemap: true' in sitemap.

- og:image  - needs to be in '/assets/images/og_image.jpg'
dimensions: 1200px x 1200px
use site logo for graphics.
note that ONLY .jpg is supported

- Favicon - generate favicon pack with https://realfavicongenerator.net/
and put the contents of downloaded archive to '/assets/images/favicon' folder.

- Deferred image loading (lazy loading), for optimizing page load times.
For images that are set as background-image in CSS:
- add 'lazy-background' class to element that has background-image property.
For images that are placed directly in <img> tag:
- add class='lazy' to img tag.
- set src to '/assets/images/placeholder_img.svg'
- set data-src to a real image source
Example:
```HTML
<img src="/assets/images/placeholder_img.svg" data-src="/assets/images/home/laptop.svg" alt="laptop"
     class="lazy">
```

- contact form custom HTML5 warnings ('name cannot be blank' etc.) and advanced debugging.
required warnings now can be found in language YAML under contact: err_*

- contact form now:
- Does not try to invoke php on local testing (will fail as jekyll won't run php by default)
instead it will show a prompt popup to set desired form status (for easier CSS styling and debug)
- On live servers the php side will now return status, security protocol, values passed and error status if the operation has failed. This can be seen in network tab of Chrome dev tools in XHR requests. the transport script is wrapped in try / catch to avoid 500 status errors on server.

- Opening panda popup now blocks vertical scroll
