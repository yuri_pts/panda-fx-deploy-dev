---
exclude_from_sitemap: true
---

<?php
/* PHP sees the root from global scope, so  make sure that you have correct path. */
require_once "/var/www/{{ site.brand_platform }}/{% if site.deployEnv == "dev" %}dev{% else %}prod{% endif %}/swiftmailer/lib/swift_required.php";

/* Check if request comes from the correct site to avoid cross-site spam attacks. */
function checkReferer() {
	$domain = "{{ site.domain }}";
	$patterns = ["/(http(s)?:\/\/)/","/\/(.*)$/","/www\./","/dev\./"];
	$referer = preg_replace($patterns, '', $_SERVER['HTTP_REFERER']);
	$domain = preg_replace($patterns, '', $domain);
	return $referer == $domain;
}

if (checkReferer() !== true ) {
    echo "wrong referrer, message not sent";
	die();
}

try {
    /* Enable for host IP resolving
     $smtp_host_ip = gethostbyname('{{ site.SMTP.host }}');

     Create the Transport */
    $transport = Swift_SmtpTransport::newInstance()
        /* ->setHost($smtp_host_ip) */
        ->setHost("{{ site.SMTP.host }}")
        /* 587 for TLS, 465 for SSL depending on host. If you get
            Uncaught exception 'Swift_TransportException' with message 'Connection could not be established' -
            try changing encryption type */
        ->setPort({{ site.SMTP.port }})
        ->setEncryption("{{ site.SMTP.encrypt }}") /* tls or ssl */
        ->setUsername("{{ site.SMTP.user }}")
        ->setPassword("{{ site.SMTP.pass }}");
        /* enable if you're having troubles with connection establishment
        ->setStreamOptions(array('tls' => array('allow_self_signed' => true, 'verify_peer' => false)));

    Create the Mailer using your created Transport */
    $mailer = Swift_Mailer::newInstance($transport);
    echo "transport created" . PHP_EOL;
    echo "using {{ site.SMTP.encrypt }}" . PHP_EOL;

    /* Define variables from received POST */
    if(isset($_POST)){
        $name = strip_tags($_POST["name"]);
        $email = strip_tags($_POST["email"]);
        $subject = strip_tags($_POST["subject"]);
        $message = strip_tags($_POST["message"]);
        echo "data received" 	    . PHP_EOL .
            "name: " 	. $name 	. PHP_EOL .
            "email: " 	. $email 	. PHP_EOL .
            "phone: " 	. $phone  	. PHP_EOL .
            "message: " . $message 	. PHP_EOL;
    }

    /* Set timezone depending on company location */
    date_default_timezone_set("Europe/Tirane");
    $time = date("h:i:sa");

    /* Compose the message */
    $message = (new Swift_Message("New message from {{ site.brand_name }} Contact Us page, received at " . $time))
        ->setFrom("{{site.SMTP.from }}")
        ->setTo("{{ site.SMTP.send_to }}")
        ->setBcc("{{ site.SMTP.bcc_test }}")
        ->setBody(
            "Name: "      . $name . "<br />" .
            "Email: "     . $email . "<br />" .
            "Subject: "   . $subject . "<br />" .
            "Message: "   . $message  . "<br />",
            "text/html"
        );

    /* Send the message */
    $result = $mailer->send($message);
    echo "message was sent successfully" . PHP_EOL;

    /* Compose autoreply */
    $message = (new Swift_Message("{{ site.brand_name }} t autoreply.subj-title"))
        ->setFrom("{{ site.SMTP.from }}")
        ->setTo($email)
        ->setBcc("{{ site.SMTP.bcc_test }}")
        ->setBody(
            "<p> t autoreply.body-greeting " . $name . "!</p>" .
            "<p>t autoreply.body-1stline" .
            "<br> t autoreply.body-2ndline .</p>", "text/html");

    /* Send the autoreply */
    $result = $mailer->send($message);
    echo "-- SEND SUCCESS! --";
    http_response_code(201);

} catch (\Exception $e) {
    echo "-- SEND FAILED! --" . PHP_EOL;
    echo $e->getMessage();
    http_response_code(500);
}

/* For troubleshooting - refer to php5-fpm.log located in :root/logs folder in FTP */