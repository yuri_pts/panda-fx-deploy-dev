---
---
'use strict';
//Liquid to Angular escape start (raw)
{% raw %}
var currentLang = 'en',
    scrollContainer = document.querySelector('.ps-container'),
    ps = new PerfectScrollbar(scrollContainer),
    translations = currentLang;

var tradingAssetsApp = angular.module('tradingAssetsApp', ['pascalprecht.translate']);

var mapIcons = {};
$.getJSON( "/assets/trading-assets-list-plugin/js/mapIcons.json", function( data ) {
    mapIcons = data;
});

tradingAssetsApp.constant('categoriesList', [
    {id: 'FOREX', name: 'forex'},
    {id: 'CRYPTO', name: 'crypto'},
    {id: 'COMMODITIES', name: 'commodities'},
    {id: 'SHARES', name: 'stocks'},
    {id: 'INDICES', name: 'indices'}
]);

angular.module('tradingAssetsApp').config(['$translateProvider',
    function($translateProvider) {
        var language = currentLang;

        $translateProvider.useStaticFilesLoader({
            prefix: '/assets/trading-assets-list-plugin/locales/',
            suffix: '.json'
        });

        $translateProvider.preferredLanguage('en');
        $translateProvider.fallbackLanguage('en');
        $translateProvider.forceAsyncReload(true);
        $translateProvider.useSanitizeValueStrategy('sceParameters');
    }
]);

tradingAssetsApp.controller('TradingAssetsController', ['$translate', '$http', 'categoriesList', '$scope',

    function TradingAssetsController($translate, $http, categoriesList, $scope) {
        $translate.use(currentLang);
        $scope.categories = {};
        $scope.categoriesList = categoriesList;
        //$scope.daysOfWeek = daysOfWeek;
        $scope.selectedCategory = "";

        $scope.fadoutClass = false;

        $scope.changeCategory = function (category) {
            $scope.loaderSpin = true; // Show spinner
            $scope.fadoutClass = true;

            if ($scope.categories[category] && $scope.categories[category].length > 0) {
                $scope.selectedCategory = category;

                $scope.fadoutClass = false;

                return;
            }
            $scope.activeItem = null;
            $scope.assets = [];
            // Count 5 minutes and change url parameter '&ast' to prevent getting probably outdated cached data
            var i = 0;
            setInterval(function () {
                i += 1;
            }, 5 * 60 * 1000);
            // MAKE SURE to put correct URL per brand // Angular escape : andraw used in order to bring liquid variable from _config.yml file (site.brand_platform)
            $http.get('https://{% endraw %}{{ site.brand_platform }}{% raw %}.pandats-client.io/assets?assetcategory=' + category + '&ast=' + i, {
                cache: true
            })
                .then(function (response) {
                    $scope.assets = response.data.map(function (asset) {
                        var symbolSubst = {
                            bab: 'btc',
                            bsv:'btc',
                            xauusd: 'gold'
                        }

                        function replaceSubst(string) {
                            let sanitized = string.replace(/ |-|_/g,'');
                            if (sanitized in mapIcons) {
                                return (mapIcons[sanitized]);
                            } else {
                                return sanitized;
                            }
                        }

                        var flagname1 = '';
                        var flagname2 = '';
                        var flagname3 = '';
                        switch (category) {
                            case 'FOREX':
                            case 'CRYPTO':
                                flagname1 =  replaceSubst(asset.Name.substring(0, 3).toLocaleLowerCase());
                                flagname2 =   replaceSubst(asset.Name.substring(3, 6).toLocaleLowerCase());
                                break;
                            default:
                                flagname3 =  replaceSubst(asset.Name.toLocaleLowerCase());
                                break;
                        }

                        asset.active = false;
                        asset.flag1 = flagname1;
                        asset.flag2 = flagname2;
                        asset.flag3 =  flagname3;
                        asset.Ask = asset.Ask.toFixed(asset.Digits);
                        asset.Bid = asset.Bid.toFixed(asset.Digits);
                        return asset;
                    });

                    $scope.activeItem = $scope.assets[0];
                    $scope.tradingHours = $scope.assets[0].TradingTimes[1];

                    var tradingHoursArray = $scope.tradingHours.split("-");
                    tradingHoursArray[1].trim() === "24:00" ? tradingHoursArray[1] = "23:59" : tradingHoursArray[1];

                    $scope.tradingHours = tradingHoursArray.join(" - ");
                    $scope.assets[0].TradingTimes[6] === undefined ? $scope.sundayTradingHours = translations.closed : '';
                    $scope.noCategoryAssets = false;
                    $scope.loaderSpin = false; // Hide spinner
                }).catch(function (error) {
                $scope.selectedCategory = [];
                $scope.categories[category] = [];
                $scope.noCategoryAssets = true;
                $scope.loaderSpin = false; // Hide spinner
                return false;
            });

            $scope.selectedCategory = category;

            scrollContainer.scrollTop = 0; // Scroll to top when switch asset category tab
        };

        $scope.sundayTradingHours = '';
        $scope.playAccordion = function (el) {

            if ($scope.activeItem === el) {
                el.active = !el.active;
            } else {
                $scope.activeItem.active = false;
                $scope.activeItem = el;
                $scope.activeItem.active = true;
            }


            function initAccordion(accordionElem) {
                var elementYPosition,
                    containerYPosition;

                function expandAssetInfo(event) {
                    var expandedPanel = accordionElem.querySelector(".active"), // Clicked asset
                        expandedPanelScrollToTop,
                        targetScroll; // Distance of Y axis scrolling to get the top of container

                    if (expandedPanel) {
                        if (expandedPanel === event.currentTarget) { // If clicked asset is already opened
                            // Close it
                        } else { // Clicked asset isn't expanded already
                            elementYPosition = event.currentTarget.getBoundingClientRect().top; // Y axis position of clicked element relative to the viewport
                            expandedPanelScrollToTop = expandedPanel.getBoundingClientRect().top; // Y axis position of already opened element relative to the viewport
                            containerYPosition = scrollContainer.getBoundingClientRect().top; // Y axis position of container relative to the viewport
                            // Close opened asset

                            if (expandedPanelScrollToTop < elementYPosition) { // Define relative Y axis position between already opened and just clicked assets
                                containerYPosition = scrollContainer.getBoundingClientRect().top;
                                targetScroll = elementYPosition - containerYPosition - expandedPanel.offsetHeight + event.currentTarget.offsetHeight;

                                scrollAssetToTop(targetScroll);
                            } else {
                                targetScroll = elementYPosition - containerYPosition;
                                scrollAssetToTop(targetScroll);
                            }
                        }
                    } else { // No opened assets yet. Congrats, you're the first!
                        containerYPosition = scrollContainer.getBoundingClientRect().top;
                        elementYPosition = event.currentTarget.getBoundingClientRect().top;
                        targetScroll = elementYPosition - containerYPosition;
                        scrollAssetToTop(targetScroll);
                    }
                }

                expandAssetInfo(event);
            }

            function scrollAssetToTop(targetScroll) { // Smooth scroll
                for (var i = 0; i < targetScroll; i++) {
                    (function (i) {
                        setTimeout(function () {
                            scrollContainer.scrollTop++;
                        }, i * 3);
                    })(i);
                }
            }

            initAccordion(document.getElementById("trading-assets-list"));
        };

        function initAssetsListApp() {
            $scope.changeCategory($scope.categoriesList[0].id);
        }

        initAssetsListApp();

    }
]).directive('errSrc', function () { // Fallback asset icon to default one
    return {
        link: function (scope, element, attrs) {
            var watcher = scope.$watch(function () {
                return attrs['ngSrc'];
            }, function (value) {
                if (!value) {
                    element.attr('src', attrs.errSrc);
                }
            });

            element.bind('error', function () {
                element.attr('src', attrs.errSrc);
            });

            element.bind('load', watcher);
            element.on('error', function () {
                return false
            })
        }
    }
});
{% endraw %}