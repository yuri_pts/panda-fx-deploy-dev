 $(document).ready(function () {
    awesomeForm('contactForm', 'contact_button');
});

function awesomeForm(id, buttons) {
    let $id = $('#' + id),
        $button = $('#' + buttons),
        waitButtonMsg = ' t contact.wait';
    const initButtonMsg = $button.attr('value');

    function lockElem() {
        $button.attr('value', waitButtonMsg);
        $id.css({'pointer-events': 'none'});
    }

    function unlockElem() {
        $button.attr('value', initButtonMsg);
        $id.css({'pointer-events': 'auto'});
    }

    function customValidity(ele, message) {
        $('#'+ id + ' ' + ele)
            .on('invalid', function () {
                return this.setCustomValidity(message);
            })
            .on('input', function () {
                return this.setCustomValidity('');
            });
    }

    $id.submit(function (form) {
        let myForm = document.getElementById(id),
            formData = new FormData(myForm),
            $formFail = $('.send_form_fail'),
            $formSuccess = $('.send_form_success');

        function showError() {
            //console.log('Send fail!');
            $formFail.fadeIn();
            setTimeout(function () {
                $formFail.fadeOut();
            }, 7000);
        }

        function showSuccess() {
            //console.log('Send succeess!');
            $formSuccess.fadeIn();
            $id[0].reset();
            setTimeout(function () {
                $formSuccess.fadeOut();
            }, 7000);
        }

        if (isPHP === false) {
            form.preventDefault();
            let formState = parseInt(prompt("PHP is disabled in localhost! \n" +
                                                    "1 for success, 2 for error, 3 for waiting:",
                                    "1"));
            switch (formState) {
                case 1:
                    showSuccess();
                    break;
                case 2:
                    showError();
                    break;
                case 3:
                    lockElem();
                    break;
            }

        } else {
            form.preventDefault();
            $.ajax({
                type: 'POST',
                async: true,
                url: '/assets/php/contact-form.php',
                data: formData,
                datatype: 'json',
                processData: false,
                contentType: false,
                cache: true,
                global: false,
                beforeSend: function () {
                    $formSuccess.hide();
                    $formFail.hide();
                    lockElem($button);
                },
                error: showError,
                success: showSuccess,
                complete: function () {
                    //console.log('done!');
                    unlockElem($button);
                }
            });
        }
    });
};
