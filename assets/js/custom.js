demoModalHandle();
bodyLoginClassHandler();

/* Main jQuery ready trigger */
$(function () {
    //checkloginState();
    //Set login / logout class to body
    // Route CTA button behavior
    //ctaBtnProcessing();
    //mobileDevice();
    // Toggle .hover class in mobile sidemenu
    toggleSubmenu($('#slide-out .has_children'));

    //Hook materialize collapsible tabs
    $('.collapsible').collapsible();

    $('.pin-header#Header').pushpin({
        top: headerHeight,
        offset: 0,
        onPositionChange: function(string) { handleTopNav(string);}
    });

    $('.scrollspy').scrollSpy({
        scrollOffset: 200
    });

    //Mobile menu init.
    $('.sidenav').sidenav({
        edge: 'right',
        preventScrolling: true,
        onOpenStart: function () {
            $('body').addClass('nav-active')
        },
        onCloseStart: function () {
            $('body').removeClass('nav-active')
        }
    });
    //NFP Lang Class According To NFP Language
    if(window.location.href.indexOf("?lang=") > -1) {
        var str = urlQueryString;
        var res = str.split('?lang=');
        $('body').addClass(res[1]);
    }

});

let headerHeight = $('.header').outerHeight(),
    footerAndSideMenuHeight = $( 'footer' ).height() + $( '.navbar-nav-menu' ).outerHeight(),
    bottomVal = $( document ).height() - footerAndSideMenuHeight - headerHeight,
    innerTitleHeight = $('.inner_title').outerHeight();

// Remove Loading bar
function loadingBarRemove() {
    $('.loader_animation').removeClass('loader_animation');
}


//Set Local storage on switch language
function setPreferredLang(selectedLang) {
    //console.log('Set local storage: ', selectedLang);
    localStorage.setItem("userPreferredLang", selectedLang);
}

//Login state body class handler
function bodyLoginClassHandler() {
    if (isLoggedIn === false) {
        document.body.classList.remove('logged_in');
        document.body.classList.add('logged_out');
    } else {
        document.body.classList.remove('logged_out');
        document.body.classList.add('logged_in');
    }
}

function mobileDevice() {
    if (isMobile === false) {
        $('body').addClass('desktop_body');

    } else {
        $('body').addClass('mobile_body');
        $('.lock_load').removeClass('lock_load');
    }
}

// Mobile menu nesting functionality
function toggleSubmenu($element) {
    $element.click(function () {
        $(this).siblings().removeClass('open');
        $(this).toggleClass('open');
    });
}

function ctaBtnProcessing(trigger) {
    let langSlash = '';
    langPath !== '/' ? langSlash = '/' : langSlash = '';

    let $ctaButton = $('.js_cta_button'),
        mobileSignupHref = '/mobile/?lang=' + siteLang + '#signup',
        mobileLoginHref = '/mobile/?lang=' + siteLang + '#login',
        tradeRoomHref = langPath + langSlash + 'trade-room';

    switch (isMobile) {
        case false:
            switch (trigger) {
                case 'login':
                    switch (isLoggedIn) {
                        case false:
                            runPlugin("forexLogin");
                            break;
                        case true:
                            window.location.href = tradeRoomHref;
                            break;
                    }
                    break;
                case 'signup':
                    switch (isLoggedIn) {
                        case false:
                            runPlugin("forexSignup");
                            break;
                        case true:
                            window.location.href = tradeRoomHref;
                            break;
                    }
                    break;
            }
            break;

        case true:
            switch (trigger) {
                case 'login':
                    window.location.href = mobileLoginHref;
                    break;
                case 'signup':
                    window.location.href = mobileSignupHref;
                    break;
            }
            break;
    }
}

function demoModalHandle(caller) {
    if(document.getElementById('modal_fullscreen')) {
        let demoModal = document.getElementById('modal_fullscreen'),
            demoButton = document.getElementById('demo_close'),
            isBannerDismissed = sessionStorage.getItem('demo_modal');

        if( typeof caller === "undefined" ) {
            if( !isBannerDismissed ) {
                demoModal.style.opacity = "1";
                document.body.classList.add('body-no-scroll-pandats');
            } else {
                demoModal.style.display = "none";
            }
        } else if( caller === 'button' ) {
            isBannerDismissed = sessionStorage.setItem('demo_modal', 'dismissed');
            demoModal.style.opacity = "0";
            document.body.classList.remove('body-no-scroll-pandats');
            setTimeout(function(){
                demoModal.style.display = "none";
            }, 600);
        }
    }
}

function handleTopNav(string) {
    let targetDiv = document.getElementById('install-prompt-box');
    switch(string) {
        case 'pinned':
        break;
        case 'pin-top':
            if( typeof targetDiv  !== 'undefined') {
                targetDiv.classList.add('fading');
                setTimeout(function(){
                    targetDiv.classList.remove('fading');
                }, 1000 );
            }
        break;
    }
}

//Lazy Image
document.addEventListener("DOMContentLoaded", function () {
    var lazyImages = [].slice.call(document.querySelectorAll("img.lazy")),
        lazyBackgrounds = [].slice.call(document.querySelectorAll(".lazy-background"));

    if ("IntersectionObserver" in window && "IntersectionObserverEntry" in window && "intersectionRatio" in window.IntersectionObserverEntry.prototype) {
        var lazyImageObserver = new IntersectionObserver(function (entries, observer) {
            entries.forEach(function (entry) {
                if (entry.isIntersecting) {
                    var lazyImage = entry.target;
                    lazyImage.src = lazyImage.dataset.src; //lazyImage.srcset = lazyImage.dataset.srcset;

                    lazyImage.classList.remove("lazy");
                    lazyImageObserver.unobserve(lazyImage);
                }
            });
        }, {
            rootMargin: '500px'
        });
        lazyImages.forEach(function (lazyImage) {
            lazyImageObserver.observe(lazyImage);
        });
        var lazyBackgroundObserver = new IntersectionObserver(function (entries, observer) {
            entries.forEach(function (entry) {
                if (entry.isIntersecting) {
                    entry.target.classList.remove("lazy-background");
                    lazyBackgroundObserver.unobserve(entry.target);
                }
            });
        }, {
            rootMargin: '500px'
        });
        lazyBackgrounds.forEach(function (lazyBackground) {
            lazyBackgroundObserver.observe(lazyBackground);
        });
    }
});
//Lazy Image

//Panda platform events

function depositSuccessCallback(event) {
    //console.log('deposit callback event:', event);
}

function depositFailCallback(event) {
    //console.log('deposit fail callback event:', event);
}

function signupSuccessCallback(event) {
    //console.log('signup callback event:', event);
    checkloginState();
    bodyLoginClassHandler();
}

function signupFailCallback(event) {
    //console.log('signup fail callback event:', event);
    checkloginState();
    bodyLoginClassHandler();
}

function loginSuccessCallback(event) {
    //console.log('login success callback');
    isLoggedIn = true;
    forexEvents.loggedIn = true;
    if (location.search.match(/redirect=deposit/i)) return runPlugin("forexDeposit");
    checkloginState();
    bodyLoginClassHandler();

}

function loginFailCallback(event) {
    //console.log('login fail callback event', event);
}

function logoutCallback(event) {
    //console.log('logout callback event', event);
    isLoggedIn = false;
    location.reload();
}

function themeChangeCallback(event) {
    //console.log('theme change callback event', event);

    (function callBackSwitch(callbackEvent) {
        if (callbackEvent.theme === 'theme-dark') {
            $('body').addClass('theme-dark').removeClass('theme-white');
        } else if (callbackEvent.theme === 'theme-white') {
            $('body').addClass('theme-white').removeClass('theme-dark');
        }
    })(event);
}

function appInitCallback(event) {
    //console.log('app init callback event', event);
    bodyLoginClassHandler();
    initCookieManager();
    launchCookiePopup();
    loadingBarRemove();

    $('.lock_load').removeClass('lock_load');

    if (typeof changeThemeColorCHKBX === "function") {
        changeThemeColorCHKBX('load');
    }

    if (location.search.match(/redirect=deposit/i)) {
        if (!localStorage.getItem("panda-forex__token")) return runPlugin("forexLogin");
        if (forexEvents.loggedIn) return runPlugin("forexDeposit");
    }
}

/*var forexEvents = {
    depositSuccess: depositSuccessCallback,
    depositFail: depositFailCallback,
    signupSuccess: signupSuccessCallback,
    signupFail: signupFailCallback,
    loginSuccess: loginSuccessCallback,
    loginFail: loginFailCallback,
    logout: logoutCallback,
    init: appInitCallback,
    themeChange: themeChangeCallback
};*/

$(window).scroll(function () {
    if ($(this).scrollTop() > 150) {
        $('#to-top-button').fadeIn();
        $('#social-btn').addClass('active');
    } else {
        $('#to-top-button').fadeOut();
        $('#social-btn').removeClass('active');
    }
});

$('#to-top-button').click(function () {
    $('body,html').animate({
        scrollTop: 0
    }, 1000);
    return false;
});

//Modal init.
$('#modalVideoFrame').modal({
    onOpenStart: function () {
        document.documentElement.style.setProperty('--scrollbar-width', (window.innerWidth - document.documentElement.clientWidth) + "px");
        $('body').toggleClass('scroll_compensate');
    },

    onCloseStart: function () {
        $('body').toggleClass('scroll_compensate');
        pauseVideo();
    }
});

