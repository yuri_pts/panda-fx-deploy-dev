// scripts only for Inner pages


// Carousel Initial
document.addEventListener('DOMContentLoaded', function () {
    if (document.body.contains(document.querySelectorAll('.carousel')[0])) {
        var carousel = document.querySelectorAll('.carousel')[0],
            nextCarousel = document.querySelector(".controls-holder .next-arrow"),
            prevCarousel = document.querySelector(".controls-holder .prev-arrow"),

            carouselInstance = M.Carousel.init(carousel, {
                dist: -250,
                numVisible: 5,
                fullWidth: false,
                indicators: false
            });

        nextCarousel.addEventListener('click', function (e) {
            carouselInstance.next();
        });

        prevCarousel.addEventListener('click', function (e) {
            carouselInstance.prev();
        });
    }
});

$(function() {
    $('.left_sidebar .navbar-nav-menu').pushpin({
        top: innerTitleHeight,
        bottom: bottomVal,
        offset: 20
    });

    $('.centered-nav .navbar-nav-menu').pushpin({
        top: innerTitleHeight + $('.navbar-nav-menu').height(),
        bottom: bottomVal,
        offset: 20
    });

    $('.letter-list-wrap').pushpin({
        top: innerTitleHeight + $('.navbar-nav-menu').height() + $('#breadcrumbs').height() + $('.letter-list-wrap').height(),
        offset: headerHeight + $('.navbar-nav-menu').height() -10
    });
});
