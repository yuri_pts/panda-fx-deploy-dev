function maxMind() {
    let restrictAccess = ["AL", "ZW"], // Restricted countries list
        whitelist = ["127.0.0.1", "127.0.0.2"]; // Whitelisted IP addresses list

    function grantAccess() {
        document.body.removeAttribute('style');
        document.body.classList.add('ip-allowed');
    }

    function onSuccess(geoipResponse) { // Visitor origin detected callback
        let code = geoipResponse.country.iso_code,
            ip = geoipResponse.traits.ip_address,
            denied = restrictAccess.find(function (country) {
                return country === code;
            }),
            accessGranted = whitelist.find(function (i) { //
                return i === ip;
            });

        //console.log('Country:', code);
        //console.log('IP:', ip);

        if (denied && !accessGranted) {
            location.href = "/access-denied/";
        } else {
            grantAccess();
            localStorage.setItem('accessGranted', 'true');
        }
    }

    function onError(error) {
        //console.log('IP to country failed: ' + error );
    }

    if (localStorage.getItem('accessGranted')) {
        grantAccess();

    } else {
        geoip2.country(onSuccess, onError);
    }
}

maxMind();