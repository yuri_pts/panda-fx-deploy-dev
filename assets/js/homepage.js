// scripts only for homepage
//Heros screen banner video box on mouse hover animation

$(function () {
    const parallaxBox = document.getElementById('parallaxBox');
    if ( isMobile === false ) {

        parallaxBox.addEventListener('mousemove', debounce(parallax, 5));
    }
});

function debounce(callback, interval) {
    let debounceTimeoutId;

    return function(...args) {
        clearTimeout(debounceTimeoutId);
        debounceTimeoutId = setTimeout(() => callback.apply(this, args), interval);
    };
}

function parallax (e) {
    this.querySelectorAll('.layer').forEach(layer => {
        const speed = layer.getAttribute('data-speed');
        const x = (window.innerWidth - e.pageX*speed)/100;
        const y = (window.innerHeight - e.pageY*speed)/100;

        layer.style.transform = `translateX(${x}px) translateY(${y}px)`;
    })
}

//Init AOS content animation on scroll library
AOS.init({
    easing: 'ease-in-out-sine'
});



// Feature Section Theme Switcher
$( "#theme-switcher" ).click(function() {
    $(this).toggleClass('checked')
    changeColorCHKBX('click');
});

function changeColorCHKBX(trigger) {

    if (trigger === 'click') {
        if ( $( "#theme-switcher" ).hasClass('checked') ) {
            $('#compare').css('width', '100%');

        } else {
            $('#compare').css('width', '0%');
        }
    }
}

$('.platforms-contain figure').show();
$('#compare').css('width', '50%');

function beforeAfter() {
    document.getElementById('compare').style.width = document.getElementById('slider').value + "%";
}

// Carousel Initial
document.addEventListener('DOMContentLoaded', function () {
    let carousel = document.querySelectorAll('.carousel'),
        carousel2 = document.querySelectorAll('.full-width-carousel'),
        nextCarousel = document.querySelector(".controls-holder .next-arrow"),
        prevCarousel = document.querySelector(".controls-holder .prev-arrow"),
        nextCarousel2 = document.querySelector(".controls-slider-carousel .next-arrow"),
        prevCarousel2 = document.querySelector(".controls-slider-carousel .prev-arrow"),

        carouselInstance = M.Carousel.init(carousel, {
            dist: -20,
            numVisible: 3,
            fullWidth: false,
            indicators: false
        }),
        carouselInstance2 = M.Carousel.init(carousel2, {
            numVisible: 5,
            dist: 0,
            indicators: true,
            fullWidth: false,
            padding: 10
        });

    nextCarousel.addEventListener('click', function (e) {
        var carousel = document.querySelector('.carousel');
        var instance = M.Carousel.getInstance(carousel);
        instance.next();
        e.preventDefault();
    });

    prevCarousel.addEventListener('click', function (e) {
        var carousel = document.querySelector('.carousel');
        var instance = M.Carousel.getInstance(carousel);
        instance.prev();
        e.preventDefault();

    });
    nextCarousel2.addEventListener('click', function (e) {
        var carousel2 = document.querySelector('.full-width-carousel');
        var instance = M.Carousel.getInstance(carousel2);
        instance.next();
        e.preventDefault();
    });

    prevCarousel2.addEventListener('click', function (e) {
        var carousel2 = document.querySelector('.full-width-carousel');
        var instance = M.Carousel.getInstance(carousel2);
        instance.prev();
        e.preventDefault();

    })
});
