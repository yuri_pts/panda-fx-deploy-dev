<section id="parallaxBox" class="top-hero-screen">
    <div class="hero-bg"></div>
    <div class="container">
        <div class="hero-screen-content-holder parallax">
            <!--<div class="parallax-top-left"></div>
            <div class="parallax-top-right"></div>
            <div class="parallax-bottom-left" onclick="ctaBtnProcessing('signup')" title="Open Account"></div>
            <div class="parallax-bottom-right"></div>-->
            <div class="parallax-content">
                <div class="row c-box">
                    <div class="col s12 l6 xl6">
                        <div class="left-content-box parallax-front">
                            <h1 class="editable" data-i18n="hero_screen_title">Your ultimate <br>
                                trading environment</h1>
                            <p class="editable" data-i18n="hero_screen_message">Discover {{ site.brand_name }} WebTrader, <br >
                                a next generation trading platform that provides <br />
                                the same great trading experience across <br />
                                all platforms and devices.</p>
                            <div class="cta_btn_holder editable" data-i18n="hero_screen_action">
                                <a class="cta_button cta_button1 cta_arrow bg_hover_fadein lock_load"
                                   href="javascript:void(0);" onclick="ctaBtnProcessing('login')">
                                    Start Trading
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="right-content-box col s12 l6 xl6">
                        <div class="open-order-frame layer" data-speed="1"></div>
                        <div class="trading-platform-frame layer" data-speed="3"></div>
                        <div class="mobile-phone layer" data-speed="5">
                            <video class="video-source"
                                   loop
                                   autoplay
                                   muted
                                   id="demoVideo"
                                   poster="/assets/images/home/phone-placeholder.jpg">
                                <source src="/assets/mp4/mobile-open-order-steps.mp4"
                                        type="video/mp4">
                            </video>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="slider-carousel" data-aos="fade-up">
    <div class="row">
        <div class="col s12 m12 l8 offset-l2 center">
            <h3 class="editable" data-i18n="why_trade_with_us_title">Why Trade with {{ site.brand_name }}?</h3>
            <p class="editable" data-i18n="why_trade_with_us_text">With over two decades in the business of online trading, {{ site.brand_name }} is an experienced, globally recognised name that operates under the strictest standards and is here to stay. {{ site.brand_name }} offers traders of all levels a safe and secure multi-asset trading experience on a variety of platforms.</p>
        </div>
        <div class="col s12 m12 l2 center">
            <div class="controls-slider-carousel">
                <a href="javascript:void(0)"
                   title="Navigation"
                   class="prev-arrow"><img src="/assets/images/home/prev-slider.svg"
                                           alt=""></a>
                <a href="javascript:void(0)"
                   title="Navigation"
                   class="next-arrow"><img src="/assets/images/home/next-slider.svg"
                                           alt=""></a>
            </div>
        </div>
    </div>
</section>