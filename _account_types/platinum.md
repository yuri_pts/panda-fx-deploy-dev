---
account_type_name: Platinum
minimum_deposit: $10 000
background_image: /assets/images/home/bg_basic.png
account_options:
    opt1: Minimum deposit $10 000
    opt2: Minimum spread from 0 pips
    opt3: Commission $2 per lot
    opt4: Free VPS
    opt5: Personal account manager
---