---
account_type_name: Gold
minimum_deposit: $1000
background_image: /assets/images/home/bg_silver.png
account_options:
    opt1: Minimum deposit $1000
    opt2: Minimum spread from 0.8 pips
    opt3: Leverage up to 1:250
    opt4: Micro lots available
    opt5: Free VPS
---