---
account_type_name: Silver
minimum_deposit: $250
background_image: /assets/images/home/bg_basic.png
account_options:
    opt1: Minimum deposit $250
    opt2: Minimum spread from 1.2 pips
    opt3: Leverage up to 1:100
    opt4: Micro lots available
    opt5: Free educational ebook
---